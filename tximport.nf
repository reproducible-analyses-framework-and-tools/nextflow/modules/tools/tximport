process tximport {

  label "tximport_container"
  label "tximport"

  input:
  tuple val(pat_name), val(run), val(dataset), path(quants)
  path tx2gene
  val quant_type
  val suffix

  output:
  tuple val(pat_name), val(run), val(dataset), path("*txi*${suffix}"), emit: gene_count_mtxs

  script:
  """
  echo "library(tximport)" > CMD
  echo "tx2gene <- read.table(\\"${tx2gene}\\", header=T, sep='\\t')" >> CMD
  echo "quants <- list.files(pattern=\\"${quants}\\", all.files=T)" >> CMD
  echo "names(quants) <- quants" >> CMD
  echo "txi <- tximport(files=quants, type=\\"${quant_type}\\", tx2gene=tx2gene)" >> CMD
  echo 'write.table(txi\$counts, "tmp", col.names=T, row.names=T, sep="\t", quote=F)' >> CMD
  chmod +x CMD
  Rscript CMD
  mv tmp ${dataset}-${pat_name}-${run}.txi${suffix}
  """
}
